﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Wedding.Services
{
    public class Email
    {
        private static Dictionary<string, int> getPortAndSmtpAddress(string hostMail)
        {
            var result = new Dictionary<string, int>();
            hostMail = hostMail.Substring(hostMail.IndexOf("@") + 1);
            switch (hostMail)
            {
                case "gmail.com":
                    result.Add("smtp.gmail.com", 587);
                    break;
                case "live.com":
                case "hotmail.com":
                    result.Add("smtp.live.com", 587);
                    break;
                case "office365.com":
                    result.Add("smtp.office365.com", 587);
                    break;
                case "yahoo.com":
                    result.Add("smtp.mail.yahoo.com", 465);
                    break;
                case "zoho.com":
                    result.Add("smtp.zoho.com", 465);
                    break;
                case "mail.com":
                    result.Add("smtp.mail.com", 587);
                    break;
                case "gmx.com":
                    result.Add("smtp.gmx.com", 465);
                    break;
                default:
                    break;
            }

            return result;
        }

        /// <summary>
        /// Function to send mail
        /// </summary>
        /// <param name="mailFrom">Mail From</param>
        /// <param name="mailTo">Mail To</param>
        /// <param name="subject">Subject</param>
        /// <param name="content">Content</param>
        /// <return>Return status</return>
        public static Task<bool> SendMail(string mailFrom, string mailTo, string subject, string content, string password)
        {
            return Task.Run(() => {
                try
                {
                    var smtpAndPort = getPortAndSmtpAddress(mailFrom);
                    SmtpClient emailsend;
                    MailMessage message;
                    emailsend = new SmtpClient(smtpAndPort.Keys.SingleOrDefault());
                    emailsend.Port = smtpAndPort.Values.SingleOrDefault();
                    emailsend.DeliveryMethod = SmtpDeliveryMethod.Network;
                    emailsend.Credentials = new NetworkCredential(mailFrom, password);
                    emailsend.EnableSsl = true;
                    message = new MailMessage();

                    message.From = new MailAddress(mailFrom);
                    message.To.Add(mailTo);
                    message.Subject = subject;
                    message.IsBodyHtml = true;
                    message.Body = content;
                    emailsend.SendAsync(message, null);
                }
                catch (Exception ex)
                {
                    return false;
                }
                return true;
            });
        }
    }
}