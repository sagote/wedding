﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wedding.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Gallery(string alias)
        {
            switch (alias)
            {
                case "chup-hinh-cuoi":
                    ViewBag.Title = "Chụp hình cưới";
                    ViewBag.Message = @"+ Chụp hình cưới ngoại cảnh <br/>
                                        + Chụp hình tiệc cưới<br/>
                                        Giá: 1 000 000 VNĐ / ngày chụp<br/>
                                        Số lượng ảnh chụp 380 tấm với chất lượng hình ảnh <b>rõ sắc nét</b> hình ảnh đẹp <b>trung thực</b> tông màu đẹp mà không cần phải <b>chỉnh sửa photoshop</b>.<br/>
                                        Có chỉnh sữa photoshop trước khi in hình.<br/>
                                        Quý khách có yêu cầu rữa hình cưới bên chúng tôi sẽ làm theo yêu cầu của khách, quý khách tự trả chi phí rữa ảnh.
                                        ";
                    break;
                case "dich-vu-chup-hinh-khac":
                    ViewBag.Title = "Dịch vụ chụp hình khác";
                    ViewBag.Message = @"+ Chụp hình tiệc tùng gia đình <br/>
                                        + Chụp hình sinh nhật <br/>
                                        Giá chụp 500 000 VNĐ / buổi chụp <br/>
                                        <br/>
                                        Số lượng ảnh chụp 170 tấm với chất lượng hình ảnh <b>rõ sắc nét</b> hình ảnh đẹp <b>trung thực</b> tông màu đẹp mà không cần phải chỉnh sửa photoshop.<br/>
                                        Quý khách có yêu cầu rữa hình cưới bên chúng tôi sẽ làm theo yêu cầu của khách, quý khách tự trả chi phí rữa ảnh.
                                        ";
                    break;
                case "album-cuoi":
                    ViewBag.Title = "Album cưới";
                    break;
                case "chan-dung-ngoai-canh":
                    ViewBag.Title = "Chân dung ngoại cảnh";
                    break;
                case "anh-da-ngoai":
                    ViewBag.Title = "Ảnh dã ngoại";
                    break;
                case "anh-thien-nhien":
                    ViewBag.Title = "Ảnh thiên nhiên";
                    break;
                default:
                    break;
            }            
            return View();
        }

        public ActionResult Contact()
        {            
            return View();
        }
    }
}