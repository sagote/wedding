﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Wedding.Services;

namespace Wedding.Controllers
{
    public class ContactController : ApiController
    { 
        [HttpPost]
        public async Task<HttpResponseMessage> SendContact([FromBody] JObject contact)
        {
            var result = new JObject();
            try
            {
                string content = string.Format(@"Thông tin khách liên hệ: <br/>
                                        Tên: {0} <br/>
                                        Email: {1}<br/>
                                        Điện thoại: {2} <br/>
                                        Nội dung: {3}", contact["Name"], contact["Email"], contact["Phone"], contact["Message"]);
                bool isSent = await Email.SendMail(ConfigurationManager.AppSettings["Email"].ToString(), contact["Email"].ToString(), "Thông tin liên hệ Nhiếp ảnh Thành Công", content, ConfigurationManager.AppSettings["Pass"].ToString());
                if (!isSent)
                    result = JObject.FromObject(new { state = 400, message = "Gửi email thất bại!" });
                else
                    result = JObject.FromObject(new { state = 200, message = "Gửi email thành công!" });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadGateway, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
